import React from 'react';
import { Helmet } from 'react-helmet';
import { useState } from "react";
import Cart from "./Components/Cart/Cart";
import Header from "./Components/Layout/Header";
import Meals from "./Components/Meals/Meals";
import CartProvider from "./store/CartProvider";

function App() {
  const [cartIsShown, setCartIsShown] = useState(false);

  const showCartHandler = () =>{
    setCartIsShown(true)
  }

  const hideCartHandler = () =>{
    setCartIsShown(false)
  }

  return (
    <>
      <Helmet>
        <title>{"ReactMeals"}</title>
      </Helmet>
      <CartProvider>
        {cartIsShown && <Cart onClose={hideCartHandler}/>}
        <Header onShowCart={showCartHandler}/>
        <main>
          <Meals />
        </main>
      </CartProvider>
    </>
  );
}

export default App;
